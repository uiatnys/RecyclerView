package com.example.com.staggered;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by uiatnys on 2016/12/10.
 */

public class ItemDecoration extends RecyclerView.ItemDecoration {

    public ItemDecoration() {
        super();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.top=20;
        outRect.right=10;
        outRect.left=10;

    }
}
