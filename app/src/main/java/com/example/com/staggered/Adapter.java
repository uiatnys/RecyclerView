package com.example.com.staggered;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by 99210 on 2016/12/10.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private Context context;
    private String[] urls = new String[]{"http://img5.imgtn.bdimg.com/it/u=3397389883,721473922&fm=21&gp=0.jpg"
            ,"http://img0.imgtn.bdimg.com/it/u=1112905477,2828387231&fm=21&gp=0.jpg"
            ,"http://cpc.people.com.cn/NMediaFile/2013/0808/MAIN201308081646000175538813272.jpg"
            ,"http://pic2.ooopic.com/10/45/40/97b1OOOPIC44.jpg"
            ,"http://tupian.enterdesk.com/2012/0725/gha/2/enterdesk,com,wuyuan%20%284%29.jpg"};
    private List<Integer> mHeights;

    public Adapter(Context context) {
        super();
        this.context = context;
        mHeights = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.item,null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (mHeights.size() <= position) {
            mHeights.add((int) (100 + Math.random() * 300));
        }
        ViewGroup.LayoutParams layoutParams = holder.img.getLayoutParams();
        layoutParams.height = mHeights.get(position);
        holder.img.setLayoutParams(layoutParams);
        Glide.with(context).load(urls[position%5]).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return 100;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.image)
        ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);
        }
    }
}
